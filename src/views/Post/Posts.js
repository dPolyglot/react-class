// import axios from 'axios'
import {useEffect, useState} from 'react';
import { getPosts } from '../../apiServices/allposts';
import Post from './Post'
import './Posts.css'

const Posts = () => {
    const [postData, setPostData] = useState([])

    useEffect(() => {
        let mounted = true
        getPosts()
            .then(post => {
                if(mounted){
                    setPostData(post)
                }
            })
        return () => mounted = false
    }, [])

    const deletePost = (id) => {
        const remainingPosts = postData.filter(post => id !== post.id)
        setPostData(remainingPosts)
    }
    
      
    return (
        <>
            <div className="Posts">
                <Post posts={postData} icon="delete" action={deletePost}/>
            </div> 
        </>
    )
}

export default Posts
