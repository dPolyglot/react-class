import "./Posts.css"

const Post = ({posts, icon, action}) => {

    return (
        <div>
            <h3>All posts</h3>
            {posts.map(post => 
                <li key={post.id} className="post">{post.title} 
                    <i className="material-icons" onClick={() => action(post.id)}>{icon}</i>
                </li>
            )}
        </div>
    )
}

export default Post
