import {useEffect} from 'react'
import {Route, Switch, useRouteMatch, useLocation} from 'react-router-dom'
import Login from './Login/Login'
import Register from './Register/Register'
import Header from '../../components/Header'


const AuthContainer = () => {
    let {path} = useRouteMatch()
    let location = useLocation()

    return (
        <div>
            <Header/>
            <Switch>
                <Route strict path={`${path}/login`}>
                    <Login/>
                </Route>
                <Route strict path={`${path}/register`}>
                    <Register/>
                </Route>
            </Switch>
        </div>
    )
}

export default AuthContainer
