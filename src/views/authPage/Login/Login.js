import {useEffect, useState} from 'react'
import {useHistory} from 'react-router-dom'
import './Login.css'
import Input from '../../../components/reusables/Input'
import Button from '../../../components/reusables/Button'

const Login = () => {
    let data = {}

    const setData = (key, value) => {
        data[key] = value
    }

    const logData = () => {
        console.log(data)
    }

    const [loading, setLoading] = useState(false);

    let history = useHistory();


    const loginUser = async() => {

        try{
            let res = await fetch(`http://localhost:5000/users/?email=${data.email}&password=${data.password}`)
                .catch(error => console.error(error))
            let result = await res.json();

            setLoading(true);

            // setTimeout(function(){
            //     setLoading(false);
            // }, 2000);

            console.log(result);

            if(result.length > 0){
                history.push("/posts")
            }
        }catch(error){
            
            console.log(error);
        }
    }

    // useEffect(() => {
    //     // api calls
    //     console.log("Login: I mounted")
    //     return () => console.log("Login: I unmounted")
    // }, [])
    return (
        <div className="Login">
            
            <div className="loginContainer">
                <Input 
                    inputType="text" 
                    placeholder="email" 
                    icon="email" 
                    getLoginData={setData}
                    dataKey="email"
                    label="email"
                />

                <Input 
                    inputType="password" 
                    icon="person"
                    getLoginData={setData} 
                    placeholder="password"
                    dataKey="password"
                    label="Password"
                />

                <Button action={loginUser} btnValue={loading ? 'Loading...' : 'login now'} btnWidth="100%" textColor="#fff"/>
            </div>
        </div>
    )
}

export default Login
