
const Button = ({btnValue, bgColor, textColor, btnWidth, outline, action}) => {

    const borderOutline = `1px solid ${textColor}`

    const buttonStyle = {
        cursor: 'pointer',
        backgroundColor: bgColor,
        color: outline ? textColor : '#fff',
        border: outline ? borderOutline : 0,
        display: 'block',
        fontSize: '16px',
        height: '40px',
        lineHeight: '40px',
        borderRadius: '4px',
        width: btnWidth,
        textTransform: 'uppercase',
    }
    
    return (
        <>
            <button style={buttonStyle} onClick={action}>{btnValue}</button>
        </>
    )
}

Button.defaultProps = {
    bgColor: "black"
}

export default Button
