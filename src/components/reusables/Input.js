import {useState, useEffect} from 'react'

const Input = ({label, dataKey, getLoginData, icon, inputType, placeholder}) => {
    const [text, setText] = useState('')

    const inputContainer = {
      position: 'relative'
    }

    const inputIcon = {
      position: 'absolute',
      right: '15px',
      top: '35px',
      color: 'gray',
      fontSize: '20px'
    }

    const updateText = (value) => {
      if (dataKey) getLoginData(dataKey.toLowerCase(), value)
      setText(value)
    }

    // useEffect(
    //   () => {
    //     console.log('input mounted') // componentDidMount
    //     return () => { console.log('input unmounted')} // componentDidUnmount
    //   }
    //   , [text] // componentDidUpdate
    // )

    return (
        <>
          <div style={inputContainer} className="form-control">
            <label htmlFor="text">{label}</label>
            <input 
                type={inputType} 
                placeholder={placeholder}
                value={text}
                onChange={(e) => updateText(e.target.value)}
            />  
            <i className="material-icons" style={inputIcon}>{icon}</i>
          </div>
        </>
    )
}

export default Input
