import {useEffect} from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {pathString} from "../helpers"
import Button from '../components/reusables/Button'


const Header = () => {
    let location = useLocation();
    let history = useHistory();

    // let text = pathString(location)

    const styles = {
        display: 'flex',
        justifyContent: 'space-between',
        alignContent: 'center'
    }

    const goToRoute = () => {
        if(location.pathname === "/auth/login"){
          history.push("/auth/register")
        }else{
          history.push("/auth/login")
        }
    }

    return (
        <>
          <nav style={styles}>
            <h5>Header</h5>
            <Button 
              btnValue={location.pathname === "/auth/login" ? "register" : "login"} 
              bgColor="#fff" 
              textColor="#000" 
              action={goToRoute}
              btnWidth="40%"
              outline={true}
            />
          </nav>  
        </>
    )
}

export default Header
